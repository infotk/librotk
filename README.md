# LIBROTK
Es una colección de libros técnicos, separados en distintas categorias para que podamos compartirlos mutuamente

## Licencia
Este repocitorio esta bajo licencia GPL3 de gnu pudes consularlo aquí [licencia][licencia]

## Control de cambios
Pudes seguir los cambios en las diferentes versiones en nuestro [changelog][changelog]

## Codigo de conducta
Para pertenecer a nuestra comunidad debes tener en cuenta el siguiente [codigo de conducta][codigo]

## Como contribuir
Si te gustaria conribuir, podes hacerlo siguiendo estos consejos [como contribuir][contribuir]

## Autores
Estos son los principales autores y contribuyentes al proyecto [autores][autores]


[licencia]:LICENSE
[autores]: AUTHORS.md
[changelog]: CHANGELOG.md
[codigo]: CODE_OF_CONDUCT.md
[contribuir]: CONTRIBUTING.md