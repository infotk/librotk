# Como contribuir

Para poder contribuir tienes que seguir los siguientes pasos:
+ Crea una rama con tu actualización
+ Cuando tengas todos los cambios que quieras agregar solicita un merge
+ Respeta el [codigo de conducta][codigo]

[codigo]: CODE_OF_CONDUCT.md
