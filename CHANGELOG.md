# Changelog

Control de cambios realizados en las distintas versiones 
Formato de descripción de cambios basado en  [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
Numeración de versiones basado en [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

Las versiones se listaran de la mas actual a la mas antigua, al final del documento habrá un listado con las referencias a todas las versiones 

* Numeración de versiones **A.B.C**: 
	+ **A** indicara si hay nuevas categorías de libros
	+ **B** la incorporación de nuevos libros
	+ **C** cambios menores, nombre u ordern que no afectan el contenido

## UNRELEASED

### Added

* Creación del proyecto
* Selección de la metodología de control de versiones y versionado
* Selección de la metodología de colaboración
* Selección del tipo de licencia 


## 1.0.0 - 2022-01-13

[Unreleased]: https://gitlab.com/islandoftex/arara/compare/v6.1.5...master
[1.0]: https://gitlab.com/islandoftex/arara/-/tags/v1.0
